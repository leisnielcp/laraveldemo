@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <h3>Admin Users</h3>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="pull-right">
                                <a class="btn btn-default" href="{{ route('users.create') }}"> Create New User</a>
                            </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <hr>
                    @foreach ($users as $user)
                        <div class="row">
                            <div class="col-xs-8">
                                <span>Email {{ $user->email }}</span>
                            </div>
                            <div class="col-xs-2">
                                <a class="btn btn-default" href="{{ route('users.edit',$user->id) }}">Edit</a>
                            </div>
                            <div class="col-xs-2">
                                <form method="POST" action="{{ route('users.destroy',$user->id) }}">
                                     {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-default">Remove</button>
                                </form>    
                            </div>
                        </div>
                    @endforeach    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
